import { z } from "zod";

export const schema = z.object({
    mot: z.string().optional(),
    user: z.string().optional()
})