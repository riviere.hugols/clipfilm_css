import { z } from "zod";


export const schema = z.object({
    nom: z.string(),
    prenom: z.string(),
    pseudo: z.string(),
    mail: z.string().email(),
    naissance: z.date(),
    biographie: z.string().optional(),
    photo: z.instanceof(File).optional(),
    role: z.enum(["pro", "cinephile"]).optional(),
    password: z.string().min(8),
    password_confirm: z.string().min(8)
})