import { z } from "zod";

export const schema = z.object({
    post: z.string(),
    user: z.number(),
    date: z.date(),
    type: z.enum(["Article", "Débat", "Post"]),
    media: z.string(),
})