import { z } from "zod";

export const schema = z.object({
    ancien_password: z.string().min(8),
    new_password1: z.string().min(8),
    new_password2: z.string().min(8)
    
})