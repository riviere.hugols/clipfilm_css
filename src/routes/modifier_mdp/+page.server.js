


export async function load(event) {

}


export const actions = {
    async default(event) {
        const form = await superValidate(event, schema);
        const user = event.locals.user;
        if (!form.valid) {
            return fail(400, { form });
        }

      
        const ancien_mdp_hash = await db.get(`SELECT mdp_hash FROM Utilisateur WHERE id_utilisateur = ${user}`);
        if (await argon2.verify(ancien_mdp_hash.mdp_hash, form.data.ancien_password) && form.data.new_password1 === form.data.new_password2) {
            const new_mdp_hash = await argon2.hash(form.data.new_password1);
            let requete_modif_mdp = `UPDATE Utilisateur SET mdp_hash = '${new_mdp_hash}' WHERE id_utilisateur = ${user}`;
            await db.exec(requete_modif_mdp);
            return fail(400, { form, modifMdp: true })
        }
            
        

        
        
    },
}
