export { matchers } from './matchers.js';

export const nodes = [
	() => import('./nodes/0'),
	() => import('./nodes/1'),
	() => import('./nodes/2'),
	() => import('./nodes/3'),
	() => import('./nodes/4'),
	() => import('./nodes/5'),
	() => import('./nodes/6'),
	() => import('./nodes/7'),
	() => import('./nodes/8'),
	() => import('./nodes/9'),
	() => import('./nodes/10'),
	() => import('./nodes/11')
];

export const server_loads = [0];

export const dictionary = {
		"/": [~2],
		"/accueil_co": [~4],
		"/accueil": [~3],
		"/connexion_compte": [~5],
		"/modifier_compte": [~6],
		"/modifier_mdp": [~7],
		"/nouveau_compte": [~8],
		"/nouveau_post": [~9],
		"/page_utilisateur": [~10],
		"/recherche_utilisateur": [~11]
	};

export const hooks = {
	handleError: (({ error }) => { console.error(error) }),
};

export { default as root } from '../root.svelte';